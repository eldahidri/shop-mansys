package org.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bill")
public class Bill {

    @Id
    private int number;

    @Column(name = "product_name")
    private String productName;
    private double price;

    @Column(name = "quantity")
    public int quantityOfProduct;

    @Column(name = "total_price")
    private double totalPrice;


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantityOfProduct() {
        return quantityOfProduct;
    }

    public void setQuantityOfProduct(int quantityOfProduct) {
        this.quantityOfProduct = quantityOfProduct;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }



    public double TotalPrice(){
        totalPrice = price* quantityOfProduct;
        return  totalPrice;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "billNumber=" + number +
                ", descripe='" + productName + '\'' +
                ", price=" + price +
                ", quantity=" + quantityOfProduct +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
