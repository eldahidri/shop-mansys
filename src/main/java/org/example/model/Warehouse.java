package org.example.model;

import javax.persistence.*;

@Entity
@Table(name = "warehouse")
public class Warehouse {

@Id
@Column(name = "id")
    private int WarehouseProductId;

@Column(name = "name_of_product")
    private String nameOfProduct;

    private int quantity;

    @ManyToOne
    @JoinColumn(name = "furnitor_id")
    private Furnitor furnitor;


    public int getWarehouseProductId() {
        return WarehouseProductId;
    }

    public void setWarehouseProductId(int WarehouseProductId) {
        this.WarehouseProductId = WarehouseProductId;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Furnitor getFurnitor() {
        return furnitor;
    }

    public void setFurnitor(Furnitor furnitor) {
        this.furnitor = furnitor;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "WarehouseProductId=" + WarehouseProductId +
                ", nameOfProduct='" + nameOfProduct + '\'' +
                ", quantity=" + quantity +
                ", furnitor=" + furnitor +
                '}';
    }
}
