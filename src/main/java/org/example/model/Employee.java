package org.example.model;

import org.example.enums.JobPosition;

import javax.persistence.*;

@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @Column(name = "employee_id")
    private int employeeId;

    private String name;

    private String surname;

    @Column(name = "job_position")
    @Enumerated(EnumType.STRING)
    private JobPosition jobPosition;

    @Column(name = "phone_number")
    private String  phoneNumber;

    public int getEmployeeId() {
        return employeeId;
    }
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {

        this.name = name;

    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public JobPosition getJobPosition() {
        return jobPosition;
    }
    public void setJobPosition(JobPosition jobPosition) {
        this.jobPosition = jobPosition;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String  phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + employeeId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", jobPosition='" + jobPosition + '\'' +
                ", phoneNumber=" + phoneNumber +
                '}';
    }
}