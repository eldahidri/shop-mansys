package org.example.model;

import org.example.enums.Department;


import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product  {

    @Id
    private int id;

    @Column(name = "name_of_product")
    private String nameOfProduct;

    private double price;

    @Enumerated(EnumType.STRING)
    private Department department;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", nameOfProduct='" + nameOfProduct + '\'' +
                ", price=" + price +
                ", department=" + department +
                '}';
    }


}
