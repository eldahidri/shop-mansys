package org.example.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "furnitors")

public class Furnitor {

    @Id
    @Column(name = "furnitor_id")
    private int furnitorId;

       @Column(name = "name_of_company")
        private String nameOfCompany;

       @Column(name = "phone_number")
        private String phoneNumber;

        private String email;

        private String adress;


    @OneToMany(mappedBy = "furnitor",cascade = CascadeType.PERSIST)
    private Set<Warehouse> warehouseProduct = new HashSet<>();


        public int getFurnitorId() {
            return furnitorId;
        }

        public void setFurnitorId(int furnitorId) {
            this.furnitorId = furnitorId;
        }

        public String getNameOfCompany() {
            return nameOfCompany;
        }

        public void setNameOfCompany(String nameOfCompany) {
            this.nameOfCompany = nameOfCompany;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAdress() {
            return adress;
        }

        public void setAdress(String adress){
            this.adress = adress;
        }

    public Set<Warehouse> getWarehouseProduct() {
        return warehouseProduct;
    }

    public void setWarehouseProduct(Set<Warehouse> warehouseProduct) {
        this.warehouseProduct = warehouseProduct;
    }

    @Override
    public String toString() {
        return "Furnitor{" +
                "furnitorId=" + furnitorId +
                ", nameOfCompany='" + nameOfCompany + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", adress='" + adress + '\'' +
                ", warehouseProduct=" + warehouseProduct +
                '}';
    }

    public void add(Warehouse warehouse) {
            warehouse.setFurnitor(this);
            this.warehouseProduct.add(warehouse);
    }
    public void remove(Warehouse warehouse){
        this.warehouseProduct.remove(warehouse);
        warehouse.setFurnitor(null);
    }
}

