package org.example;

import org.example.enums.Department;
import org.example.enums.JobPosition;
import org.example.model.*;
import org.example.repository.ShopRepository;


import java.util.List;


public class ShopManagementApp {
    public static void main(String[] args) {


        // testInsertProduct();
       // testGetAllProduct();
      // testGetProductById();
     // testDeleteProduct();
    //  testUpdateProduct();

        // testInsertEmployee();
       // testGetAllEmployee();
      // testGetEmployeeById();
     // testDeleteEmployee();
    //  testUpdateEmployee();

           //testInsertProductInWarehouse();
          // testGetAllProductInWarehouse();
         // testDeleteProductInWarehouse();
        // testUpdateProductOfWarehouse();
       //  testGetProductByIdFromWarehouse();

          // testInsertFurnitor();
         // testGetAllFurnitor();
        // testGetFurnitorById();
       // testDeleteFurnitor();
      // testUpdateFurnitor();

        // testAddToTheBill();
       // testDeleteFromTheBill();
      //testUpdateBill();

    }




    private static void testInsertProduct() {
        Product product = createProduct();
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.insertProduct(product);
    }

    private static Product createProduct() {
        Product product = new Product();

        product.setId(100001);
        product.setNameOfProduct("Sugar");
        product.setPrice(0.7);
        product.setDepartment(Department.FOOD);

        product.setId(100002);
        product.setNameOfProduct("Rise");
        product.setPrice(0.5);
        product.setDepartment(Department.FOOD);

        product.setId(100003);
        product.setNameOfProduct("Water");
        product.setPrice(0.3);
        product.setDepartment(Department.DRINKS);

        product.setId(100004);
        product.setNameOfProduct("Coca-Cola");
        product.setPrice(0.6);
        product.setDepartment(Department.DRINKS);

        product.setId(100005);
        product.setNameOfProduct("Apple");
        product.setPrice(0.9);
        product.setDepartment(Department.FRUIT_VEGETABLES);

        product.setId(100006);
        product.setNameOfProduct("Banana");
        product.setPrice(1.4);
        product.setDepartment(Department.FRUIT_VEGETABLES);

        product.setId(100007);
        product.setNameOfProduct("Dash");
        product.setPrice(3.1);
        product.setDepartment(Department.DETERGENT);

        product.setId(100008);
        product.setNameOfProduct("Ariel");
        product.setPrice(4.2);
        product.setDepartment(Department.DETERGENT);

        product.setId(100009);
        product.setNameOfProduct("Chocolate Luna");
        product.setPrice(0.3);
        product.setDepartment(Department.CHOCOLATE_COOKIES);

        product.setId(100010);
        product.setNameOfProduct("Lemon Candy");
        product.setPrice(0.5);
        product.setDepartment(Department.CHOCOLATE_COOKIES);

        product.setId(100011);
        product.setNameOfProduct("Hair Shampoo");
        product.setPrice(2.4);
        product.setDepartment(Department.SKIN_CARE);

        product.setId(100012);
        product.setNameOfProduct("Lipgloss");
        product.setPrice(1.3);
        product.setDepartment(Department.SKIN_CARE);

        product.setId(100013);
        product.setNameOfProduct("Lipgloss");
        product.setPrice(2.3);
        product.setDepartment(Department.SKIN_CARE);

        product.setId(100027);
        product.setNameOfProduct("Lipgloss");
        product.setPrice(2.3);
        product.setDepartment(Department.SKIN_CARE);

        return product;
    }

    private static void testGetAllProduct() {
        ShopRepository shopRepository=new ShopRepository();
        List<Product> products=shopRepository.getAllProduct();
        System.out.println(products);

    }

    private static void testGetProductById() {
        int id=100010;
        ShopRepository shopRepository=new ShopRepository();
        Product product= shopRepository.getProductById(id);
        System.out.println(product);
    }

    private static void testDeleteProduct() {
        int id = 100027;
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.deleteProduct(id);
    }
    private static void testUpdateProduct() {
        Product product = createProductForUpdate();

        ShopRepository shopRepository = new ShopRepository();
        shopRepository.update(product);


    }


    private static Product createProductForUpdate() {
        Product product=new Product();
        product.setId(100011);
        product.setNameOfProduct("Hair Shampoo");
        product.setPrice(3.4);
        product.setDepartment(Department.SKIN_CARE);


        return product;
    }


    private static void testInsertEmployee() {
        Employee employee = createEmployee();
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.insertEmployee(employee);
    }

    private static Employee createEmployee() {
        Employee employee = new Employee();

        employee.setEmployeeId(1);
        employee.setName("Alma");
        employee.setSurname("Meci");
        employee.setJobPosition(JobPosition.CASHIER);
        employee.setPhoneNumber("0684123567");

        employee.setEmployeeId(2);
        employee.setName("Agim");
        employee.setSurname("Ndreu");
        employee.setJobPosition(JobPosition.SUPERVISOR);
        employee.setPhoneNumber("0674521789");

        employee.setEmployeeId(3);
        employee.setName("Besa");
        employee.setSurname("Koci");
        employee.setJobPosition(JobPosition.SANITARY);
        employee.setPhoneNumber("0693124543");

        employee.setEmployeeId(4);
        employee.setName("Eno");
        employee.setSurname("Lika");
        employee.setJobPosition(JobPosition.STORE_MANAGER);
        employee.setPhoneNumber("0683126587");

        employee.setEmployeeId(5);
        employee.setName("Enisa");
        employee.setSurname("Gashi");
        employee.setJobPosition(JobPosition.SHOP_ASSISTANT);
        employee.setPhoneNumber("0694455678");

        employee.setEmployeeId(6);
        employee.setName("Enisa");
        employee.setSurname("Gashi");
        employee.setJobPosition(JobPosition.SHOP_ASSISTANT);
        employee.setPhoneNumber("0694455678");

        return employee;
    }
    private static void testGetAllEmployee() {
        ShopRepository shopRepository=new ShopRepository();
        List<Employee> employees=shopRepository.getAllEmployee();
        System.out.println(employees);
    }

    private static void testGetEmployeeById(){
        int id=4;
        ShopRepository shopRepository=new ShopRepository();
        Employee employee=shopRepository.getEmployeeById(id);
        System.out.println(employee);
    }

    private static void testDeleteEmployee() {
        int employeeId = 6;
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.deleteEmployee(employeeId);
    }
    private static void testUpdateEmployee() {

            Employee employee = createEmployeeForUpdate();

            ShopRepository shopRepository = new ShopRepository();
            shopRepository.update(employee);


        }

    private static Employee createEmployeeForUpdate() {
        Employee employee=new Employee();
        employee.setEmployeeId(1);
        employee.setName("Alma");
        employee.setSurname("Meci");
        employee.setJobPosition(JobPosition.CASHIER);
        employee.setPhoneNumber("0684123000");

        return employee;
    }



    private static void testInsertProductInWarehouse() {
        Warehouse productOfWarehouse = createProductOfWarehouse();
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.insertProductForWarehouse(productOfWarehouse);
    }

    private static Warehouse createProductOfWarehouse() {
        Warehouse productOfWarehouse = new Warehouse();

        productOfWarehouse.setWarehouseProductId(100001);
        productOfWarehouse.setNameOfProduct("Sugar");
        productOfWarehouse.setQuantity(15);

        productOfWarehouse.setWarehouseProductId(100004);
        productOfWarehouse.setNameOfProduct("Coca-Cola");
        productOfWarehouse.setQuantity(18);

        productOfWarehouse.setWarehouseProductId(100006);
        productOfWarehouse.setNameOfProduct("Banana");
        productOfWarehouse.setQuantity(12);

        productOfWarehouse.setWarehouseProductId(100011);
        productOfWarehouse.setNameOfProduct("Hair Shampoo");
        productOfWarehouse.setQuantity(22);

        productOfWarehouse.setWarehouseProductId(100008);
        productOfWarehouse.setNameOfProduct("Ariel");
        productOfWarehouse.setQuantity(14);

        productOfWarehouse.setWarehouseProductId(1000019);
        productOfWarehouse.setNameOfProduct("Ho");
        productOfWarehouse.setQuantity(2);


        return productOfWarehouse;
    }

    private static void testDeleteProductInWarehouse() {
        int WarehouseProductId = 100011;
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.deleteProductFromWarehouse(WarehouseProductId);
    }
    private static void testUpdateProductOfWarehouse() {

        Warehouse product = createProductOfWarehouseForUpdate();
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.updateProductFromWarehouse(product);


    }

    private static Warehouse createProductOfWarehouseForUpdate() {
        Warehouse product = new Warehouse();

        product.setWarehouseProductId(100011);
        product.setNameOfProduct("Hair Shampoo");
        product.setQuantity(30);

        return product;

    }

    private static void testGetAllProductInWarehouse() {
        ShopRepository shopRepository=new ShopRepository();
        List<Warehouse> warehouses=shopRepository.getAllProductInWarehouse();
        System.out.println(warehouses);
    }

    private static void testGetProductByIdFromWarehouse() {
        int WarehouseProductId = 100006;
        ShopRepository shopRepository = new ShopRepository();
        Warehouse product= shopRepository.getProductByIdFromWarehouse(WarehouseProductId);
        System.out.println(product);
    }

    private static void testInsertFurnitor() {
        Furnitor furnitor = createFurnitor();
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.insertFurnitor(furnitor);
    }
    private static Furnitor createFurnitor() {
        Furnitor furnitor = new Furnitor();

//        furnitor.setFurnitorId(1);
//        furnitor.setNameOfCompany("SHA");
//        furnitor.setPhoneNumber("0682143123");
//        furnitor.setEmail("shacompany@gmail.com");
//        furnitor.setAdress("Tirane Km 7");
//
//        Warehouse product = new Warehouse();
//        product.setWarehouseProductId(100001);
//        product.setNameOfProduct("Sugar");
//        product.setQuantity(17);
//
//        Warehouse product1 = new Warehouse();
//        product1.setWarehouseProductId(100008);
//        product1.setNameOfProduct("Ariel");
//        product1.setQuantity(15);
//
//        furnitor.add(product);
//        furnitor.add(product1);

//        furnitor.setFurnitorId(2);
//        furnitor.setNameOfCompany("Gold");
//        furnitor.setPhoneNumber("0671143543");
//        furnitor.setEmail("goldcompany@gmail.com");
//        furnitor.setAdress("Durres");


//
//        furnitor.setFurnitorId(3);
//        furnitor.setNameOfCompany("Atlas");
//        furnitor.setPhoneNumber("0698765432");
//        furnitor.setEmail("atlascompany@gmail.com");
//        furnitor.setAdress("Elbasan");
//
//        Warehouse product3 = new Warehouse();
//        product3.setWarehouseProductId(100004);
//        product3.setNameOfProduct("Coca-Cola");
//        product3.setQuantity(45);
//
//        Warehouse product4 = new Warehouse();
//        product4.setWarehouseProductId(100006);
//        product4.setNameOfProduct("Banana");
//        product4.setQuantity(18);
//
//        furnitor.add(product3);
//        furnitor.add(product4);
//
        furnitor.setFurnitorId(4);
        furnitor.setNameOfCompany("Miracle");
        furnitor.setPhoneNumber("0698765445");
        furnitor.setEmail("miraclecompany@gmail.com");
        furnitor.setAdress("Tirane");

        Warehouse product2 = new Warehouse();
        product2.setWarehouseProductId(100011);
        product2.setNameOfProduct("Hair Shampoo");
        product2.setQuantity(30);

        furnitor.add(product2);

        return furnitor;
    }
    private static void testGetAllFurnitor() {
        ShopRepository shopRepository=new ShopRepository();
        List<Furnitor> furnitors=shopRepository.getAllFurnitor();
        System.out.println(furnitors);
    }

    private static void testGetFurnitorById(){
        int id=4;
        ShopRepository shopRepository=new ShopRepository();
        Furnitor furnitor=shopRepository.getFurnitorById(id);
        System.out.println(furnitor);
    }

    private static void testDeleteFurnitor() {
        int furnitorId =1;
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.deleteFurnitor(furnitorId);
    }
    private static void testUpdateFurnitor() {

        Furnitor furnitor = createFurnitorForUpdate();

        ShopRepository shopRepository = new ShopRepository();
        shopRepository.update(furnitor);


    }

    private static Furnitor createFurnitorForUpdate() {
        Furnitor furnitor=new Furnitor();
        furnitor.setFurnitorId(4);
        furnitor.setNameOfCompany("Miracle");
        furnitor.setPhoneNumber("0698765445");
        furnitor.setEmail("miraclecompany@gmail.com");
        furnitor.setAdress("Elbasan");
        return furnitor;
    }


    private static void testAddToTheBill() {
        Bill bill = createBill();
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.insertProductToTheBill(bill);
    }

    private static Bill createBill() {
        Bill bill = new Bill();

        bill.setNumber(1);
        bill.setProductName("Sugar");
        bill.setPrice(0.7);
        bill.setQuantityOfProduct(4);
        bill.TotalPrice();

        bill.setNumber(2);
        bill.setProductName("Ariel");
        bill.setPrice(4.2);
        bill.setQuantityOfProduct(1);
        bill.TotalPrice();

        bill.setNumber(3);
        bill.setProductName("Water");
        bill.setPrice(0.3);
        bill.setQuantityOfProduct(3);
        bill.TotalPrice();

        bill.setNumber(4);
        bill.setProductName("Apple");
        bill.setPrice(0.9);
        bill.setQuantityOfProduct(4);
        bill.TotalPrice();

        bill.setNumber(5);
        bill.setProductName("Banana");
        bill.setPrice(1.4);
        bill.setQuantityOfProduct(3);
        bill.TotalPrice();

        return bill;
    }

    private static void testDeleteFromTheBill() {
        int number = 5;
        ShopRepository shopRepository = new ShopRepository();
        shopRepository.deleteProductFromTheBill(number);
    }
    private static void testUpdateBill() {
       Bill bill = createBillForUpdate();

        ShopRepository shopRepository = new ShopRepository();
        shopRepository.update(bill);


    }
    private static Bill createBillForUpdate() {
        Bill bill=new Bill();

        bill.setNumber(6);
        bill.setProductName("Ariel");
        bill.setPrice(3.2);
        bill.setQuantityOfProduct(1);
        bill.TotalPrice();

        return bill;
    }



}