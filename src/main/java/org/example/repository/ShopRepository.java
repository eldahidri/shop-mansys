package org.example.repository;

import org.example.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class ShopRepository {



    public void insertProduct(Product product) {

            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            entityManager.getTransaction().begin();

            entityManager.persist(product);

            entityManager.getTransaction().commit();
            entityManager.close();
            entityManagerFactory.close();

    }

    public List<Product> getAllProduct() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        String query="SELECT p FROM Product p";

        List<Product> resultList=entityManager.createQuery(query).getResultList();

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return resultList;
    }

    public Product getProductById(int id) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        Product product=entityManager.find(Product.class,id);

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return product;
    }

    public void deleteProduct(int id) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Product product = entityManager.find(Product.class, id);
        entityManager.remove(product);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
    public void update(Product product) {


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(product);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public void insertEmployee(Employee employee) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(employee);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();

    }
    public List<Employee> getAllEmployee() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        String query="SELECT e FROM Employee e";
        List<Employee> employeeList=entityManager.createQuery(query).getResultList();

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return employeeList;

    }

    public Employee getEmployeeById(int id) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        Employee employee=entityManager.find(Employee.class,id);

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return employee;
    }

    public void deleteEmployee(int id) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Employee employee = entityManager.find(Employee.class, id);
        entityManager.remove(employee);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
}
    public void update(Employee employee) {


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(employee);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public void insertProductForWarehouse(Warehouse productOfWarehouse) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(productOfWarehouse);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
    public List<Warehouse> getAllProductInWarehouse() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        String query="SELECT w FROM Warehouse w";

        List<Warehouse> warehouseList=entityManager.createQuery(query).getResultList();

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return warehouseList;
    }
    public void deleteProductFromWarehouse(int WarehouseProductId) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Warehouse product = entityManager.find(Warehouse.class, WarehouseProductId);
        entityManager.remove(product);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public void updateProductFromWarehouse(Warehouse product) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(product);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public Warehouse getProductByIdFromWarehouse(int WarehouseProductId) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Warehouse product = entityManager.find(Warehouse.class, WarehouseProductId);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();

        return product;
    }

    public void insertFurnitor(Furnitor furnitor) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(furnitor);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public List<Furnitor> getAllFurnitor() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        String query="SELECT f FROM Employee f";

        List<Furnitor> furnitorList=entityManager.createQuery(query).getResultList();

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return furnitorList;
    }

    public Furnitor getFurnitorById(int id) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        Furnitor furnitor=entityManager.find(Furnitor.class,id);

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        return furnitor;
    }

    public void deleteFurnitor(int furnitorId) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Furnitor furnitor = entityManager.find(Furnitor.class,furnitorId );
        entityManager.remove(furnitor);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
    public void update(Furnitor furnitor) {


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(furnitor);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }


    public void insertProductToTheBill(Bill bill) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(bill);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();

    }


    public void deleteProductFromTheBill(int number) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
       Bill bill = entityManager.find(Bill.class,number );
        entityManager.remove(bill);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();

    }
    public void update(Bill bill) {


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("shopmanagementdb-em");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(bill);

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}



