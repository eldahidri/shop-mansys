package org.example.enums;

public enum JobPosition {
    STORE_MANAGER,
    SHOP_ASSISTANT,
    CASHIER,
    SANITARY,
    SUPERVISOR
}
